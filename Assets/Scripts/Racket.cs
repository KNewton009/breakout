﻿using UnityEngine;
using System.Collections;

public class Racket : MonoBehaviour {

	// Movement Speed
	public float speed = 155;


	// Update is called once per frame
	void FixedUpdate () {
	
		// Get Horizontal Input
		float h = Input.GetAxisRaw("Horizontal");
		// Set Velocity (movement direction * speed)
		GetComponent<Rigidbody2D>().velocity = Vector2.right * h * speed;
	}


	void OnCollisionEnter2D(Collision2D col) {

		if (col.gameObject.name == "ball") {
			GameObject.Find("Main Camera").GetComponent<Level>().hitRacketSource.PlayOneShot(GameObject.Find("Main Camera").GetComponent<Level>().hitRacket);
	}
}
}
