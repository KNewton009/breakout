﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Level : MonoBehaviour {

	public AudioSource hitBrickSource;
	public AudioSource hitRacketSource;
	public AudioClip hitBrick;
	public AudioClip hitRacket;
	public int score;

	// Use this for initialization
	void Start () {
		score = 0;
	}
	
	// Update is called once per frame
	void Update () {
		GameObject.Find("Score Number").GetComponent<Text>().text = score.ToString();

	}


}

