﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Ball : MonoBehaviour {

	// Movement Speed
	public float speed = 130.0f;


	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;

	}


	float hitFactor(Vector2 ballPos, Vector2 racketPos,
		float racketWidth) {
		return (ballPos.x - racketPos.x) / racketWidth;
	}

	void OnCollisionEnter2D(Collision2D col) {
		// Hit the Racket?
		if (col.gameObject.name == "racket") {

			// Play sound
			//hitRacketSource.PlayOneShot(hitRacket);

			// Calculate hit Factor
			float x = hitFactor(transform.position,
				col.transform.position,
				col.collider.bounds.size.x);

			// Calculate direction, set length to 1
			Vector2 dir = new Vector2(x, 1).normalized;

			// Set Velocity with dir * speed
			GetComponent<Rigidbody2D>().velocity = dir * speed;
		}
	}

	void OnBecameInvisible(){
		
		SceneManager.LoadScene("Scene");
	}
}
