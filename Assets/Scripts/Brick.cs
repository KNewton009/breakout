﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D collisionInfo) {
		// Destroy the whole Block
		Destroy(gameObject);
		GameObject.Find("Main Camera").GetComponent<Level>().score += 50;
		GameObject.Find("Main Camera").GetComponent<Level>().hitBrickSource.PlayOneShot(GameObject.Find("Main Camera").GetComponent<Level>().hitBrick);

	}
}
